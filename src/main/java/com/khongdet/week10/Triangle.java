package com.khongdet.week10;

public class Triangle extends Shape{
    private double a, b, c;

    public Triangle(double a, double b, double c) {
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calArea() {
        double pyarea = (a + b + c) / 2;
        return Math.sqrt(pyarea * ((pyarea - a) * (pyarea - b) * (pyarea - c)));
    }

    public double calPerimeter() {
        return (a * a) + (b * b) - (2 * a * b) * (Math.cos(c));

    }
}

